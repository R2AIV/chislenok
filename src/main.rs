use chrono::{Datelike, Utc};
use std::fs::File;
use std::io::Read;

fn main() 
{
    let _months = vec!("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", 
                       "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");        

    let filename = "holidays.txt";
    let mut f = File::open(filename).expect("Ошибка открытия файла с фразами!");
    let mut file_data = String::new();
    let mut holidays: Vec<String> = Vec::new();
    f.read_to_string(&mut file_data).expect("Ошибка чтения файла!");

    for holiday in file_data.lines()
    {
        holidays.push(holiday.to_string());        
    }


    let now = Utc::now();
    
    let month_str = match now.month()
    {
        1 => _months[0],
        2 => _months[1],
        3 => _months[2],
        4 => _months[3],
        5 => _months[4],
        6 => _months[5],
        7 => _months[6],
        8 => _months[7],
        9 => _months[8],
        10 => _months[9],
        11 => _months[10],
        12 => _months[11],
        _ => "unknown"
    };

    let day_of_year = usize::try_from(now.ordinal()).unwrap();
    
    let cur_holiday = holidays[day_of_year].to_string();

    println!("===========================");
    println!("-----=====<( \x1b[31m{}\x1b[0m", now.year());
    println!("----====<( \x1b[32m{}\x1b[0m", month_str);
    println!("---===<( \x1b[33m{}\x1b[0m", now.day());
    // println!("--==<( \x1b[34mДень в году: {}\x1b[0m", day_of_year);
    println!("\x1b[31m-=<@\x1b[33m {} \x1b[31m@>=-\x1b[0m", cur_holiday);
    println!("===========================");
}
